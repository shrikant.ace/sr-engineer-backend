import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('UsersController (e2e)', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    afterAll(async () => {
        await app.close();
    });

    // it('/users (POST)', async () => {
    //     const createUserInput = { username: 'testuser', password: 'testpassword' };

    //     return request(app.getHttpServer())
    //         .post('/users')
    //         .send(createUserInput)
    //         .expect(HttpStatus.CREATED)
    //         .then((response) => {
    //             expect(response.body.username).toEqual(createUserInput.username);
    //             expect(response.body.password).toBeUndefined(); // Ensure password is not returned in the response
    //         });
    // });

    // it('/users/:username (GET)', async () => {
    //     const username = 'testuser';

    //     return request(app.getHttpServer())
    //         .get(`/users/${username}`)
    //         .expect(HttpStatus.OK)
    //         .then((response) => {
    //             expect(response.body.username).toEqual(username);
    //             expect(response.body.password).toBeUndefined();
    //         });
    // });

    it('/users (GET)', async () => {
        return request(app.getHttpServer())
            .get('/users')
            .expect(HttpStatus.OK)
            .then((response) => {
                expect(response.body).toBeInstanceOf(Array);
            });
    });
});
