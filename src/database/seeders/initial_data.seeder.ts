import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { User } from 'src/modules/users/entities/user.entity';

export default class InitialDataSeeder implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<void> {
        await connection
            .createQueryBuilder()
            .insert()
            .into(User)
            .values([
                { username: 'admin', email: 'admin@example.com', password: "admin@123", roles: "admin" },
                { username: 'user', email: 'user@example.com', password: "user@123", roles:"user" },

            ])
            .execute();
    }
}