import { ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext,GqlContextType } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt')
{
    // getRequest(context: ExecutionContext) {
    //     const ctx = GqlExecutionContext.create(context);
    //     return ctx.getContext().req;
    // }

    public override getRequest(context: ExecutionContext): Request {
        if (context.getType<GqlContextType>() === 'graphql') {
          const ctx = GqlExecutionContext.create(context).getContext<{ req: Request }>();
    
          return ctx.req;
        }
    
        return context.switchToHttp().getRequest<Request>();
      }
}