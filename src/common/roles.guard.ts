import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql';
import { Role } from 'src/modules/users/entities/role.enum';
import { User } from 'src/modules/users/entities/user.entity';

@Injectable()
export class RoleGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    public canActivate(context: ExecutionContext): boolean {

        const requireRoles = this.reflector.getAllAndOverride<Role[]>('roles', [
            context.getHandler(),
            context.getClass(),
        ]);
        //return true;
        // if (!requireRoles) {
        //     return true;
        // }

        const request = this.getRequest(context);
        
        return true;
        // const request = context.switchToHttp().getRequest();
        // const user= request.user;
        //console.log(user)
        const user: User = {
            username: 'Shrikant',
            roles: Role.ADMIN,
            id:1,
            password:'test'
        }
        return requireRoles.some((role) => user.roles.includes(role));


    }

    public getRequest(context: ExecutionContext): Request {
        if (context.getType<GqlContextType>() === 'graphql') {
          const ctx = GqlExecutionContext.create(context).getContext<{ req: Request }>();
          return ctx.req;
        }
    
        return context.switchToHttp().getRequest<Request>();
      }
}
