import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { UsersModule } from './modules/users/users.module';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import dbconfig from './config/ormconfig';
import { APP_GUARD } from '@nestjs/core';
import { RoleGuard } from './common/roles.guard';


@Module({
    imports: [
        TypeOrmModule.forRoot(dbconfig), // define typeorm configuration
        GraphQLModule.forRoot({
            driver: ApolloDriver,
            autoSchemaFile: 'src/graphql/schema.gql',
            playground: true,
            sortSchema: true,
        }), UsersModule, AuthModule,
    ],
    providers:[{provide: APP_GUARD, useClass:RoleGuard}]
})
export class AppModule { }
