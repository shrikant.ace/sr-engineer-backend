import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserInput } from './dto/create-user.input';
import { User } from './entities/user.entity';
import { Roles } from './roles.decorator';
import { Role } from './entities/role.enum';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Post()
    async createUser(@Body() createUserInput: CreateUserInput): Promise<User> {
        return this.usersService.create(createUserInput);
    }

    @Get(':username')
    async getUserByUsername(@Param('username') username: string): Promise<User> {
        return this.usersService.findOne(username);
    }

    @Get()
    @Roles(Role.ADMIN)
    async getAllUsers(): Promise<User[]> {
        return this.usersService.findAll();
    }
}
