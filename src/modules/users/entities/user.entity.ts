import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('users') // used for database 
@ObjectType() // for graphql
export class User {
    @PrimaryGeneratedColumn()
    @Field(type => Int)
    id: number;

    @Column()
    @Field()
    username: string;

    @Column()
    @Field()
    password: string;

    @Column({ unique: true })
    email?: string;

    @Column({ default: 'user' })
    roles?: string;
}